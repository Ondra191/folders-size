package name.bazala.ondrej.folderssize.filevisitors;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.channels.ClosedByInterruptException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Stack;

public class ToCsvSizeFileVisitor implements FileVisitor {
    private double minSize;
    private boolean isSizeMB;
    private Stack<Double> sizeStack;
    private double size = 0;
    private OutputStreamWriter writer;


    public ToCsvSizeFileVisitor(double minSize, boolean isSizeMB, OutputStreamWriter writer) {
        this.minSize = minSize;
        this.isSizeMB = isSizeMB;
        this.sizeStack = new Stack<>();
        this.writer = writer;
    }

    @Override
    public FileVisitResult preVisitDirectory(Object dir, BasicFileAttributes attrs) {
        sizeStack.push(size);
        size = 0;

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Object file, BasicFileAttributes attrs) throws ClosedByInterruptException {
        if (Thread.currentThread().isInterrupted()) {
            throw new ClosedByInterruptException();
        }

        size += attrs.size();
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Object file, IOException exc) {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Object dir, IOException exc) throws IOException {
        if (size >= minSize) {
            double tmp = isSizeMB ? (size / 1024) / 1024 : ((size / 1024) / 1024) / 1024;
            String sizeStr = String.format("%.2f", tmp);
            sizeStr = isSizeMB ? sizeStr + ";MB" : sizeStr + ";GB";
            String line = "\n" + dir + ";" + sizeStr;
            writer.write(line);
        }

        size = sizeStack.pop() + size;
        return FileVisitResult.CONTINUE;
    }
}
