package name.bazala.ondrej.folderssize.filevisitors;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.ClosedByInterruptException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Locale;
import java.util.Stack;

public class ToXlsxSizeFileVisitor implements FileVisitor {
    private double minSize;
    private boolean isSizeMB;
    private Stack<Double> sizeStack;
    private double size = 0;
    private Sheet sheet;
    private int countRow = -1;


    public ToXlsxSizeFileVisitor(double minSize, boolean isSizeMB, Sheet sheet) {
        this.minSize = minSize;
        this.isSizeMB = isSizeMB;
        this.sizeStack = new Stack<>();
        this.sheet = sheet;
    }

    @Override
    public FileVisitResult preVisitDirectory(Object dir, BasicFileAttributes attrs) {
        sizeStack.push(size);
        size = 0;

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Object file, BasicFileAttributes attrs) throws ClosedByInterruptException {
        if (Thread.currentThread().isInterrupted()) {
            throw new ClosedByInterruptException();
        }

        size += attrs.size();
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Object file, IOException exc) {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Object dir, IOException exc) throws IOException {
        if (size >= minSize) {
            Row row = sheet.createRow(++countRow);

            row.createCell(0).setCellValue(dir.toString());

            double tmp = isSizeMB ? (size / 1024) / 1024 : ((size / 1024) / 1024) / 1024;
            String sizeStr = String.format(Locale.US, "%.2f", tmp);
            row.createCell(1).setCellValue(Double.parseDouble(sizeStr));

            String sizeUnit = isSizeMB ? "MB" : "GB";
            row.createCell(2).setCellValue(sizeUnit);
        }

        size = sizeStack.pop() + size;
        return FileVisitResult.CONTINUE;
    }
}
