package name.bazala.ondrej.folderssize.filevisitors;

import javax.swing.tree.DefaultMutableTreeNode;
import java.io.File;
import java.io.IOException;
import java.nio.channels.ClosedByInterruptException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Stack;

public class ToTreeSizeFileVisitor implements FileVisitor {

    private double minSize;
    private boolean isSizeMB;
    private Stack<Double> sizeStack;
    private double size = 0;
    private DefaultMutableTreeNode currentNode;


    public ToTreeSizeFileVisitor(double minSize, boolean isSizeMB, DefaultMutableTreeNode root) {
        this.minSize = minSize;
        this.isSizeMB = isSizeMB;
        sizeStack = new Stack<>();
        currentNode = root;
    }


    @Override
    public FileVisitResult preVisitDirectory(Object dir, BasicFileAttributes attrs) {
        sizeStack.push(size);
        size = 0;

        DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(Paths.get(dir.toString()).getFileName());
        currentNode.insert(newNode, currentNode.getChildCount());
        currentNode = newNode;

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Object file, BasicFileAttributes attrs) throws ClosedByInterruptException {
        if (Thread.currentThread().isInterrupted()) {
            throw new ClosedByInterruptException();
        }

        size += attrs.size();
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Object file, IOException exc) {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Object dir, IOException exc) {
        DefaultMutableTreeNode parent = (DefaultMutableTreeNode) currentNode.getParent();

        formatNodeText(currentNode);

        currentNode = parent;

        size = sizeStack.pop() + size;
        return FileVisitResult.CONTINUE;
    }

    private DefaultMutableTreeNode formatNodeText(DefaultMutableTreeNode currentNode) {
        if (size >= minSize) {
            double tmp = isSizeMB ? (size / 1024) / 1024 : ((size / 1024) / 1024) / 1024;
            String sizeStr = String.format("%.2f", tmp);
            sizeStr = isSizeMB ? sizeStr + " MB" : sizeStr + " GB";
            currentNode.setUserObject(currentNode + "     " + sizeStr);

        } else {
            currentNode.removeFromParent();
        }
        return currentNode;
    }

    public DefaultMutableTreeNode getRoot() {
        DefaultMutableTreeNode newRoot = (DefaultMutableTreeNode) currentNode.getChildAt(0);
        newRoot.setUserObject(currentNode.toString() + newRoot);
        return newRoot;
    }

    public double getRootSize() {
        return size;
    }
}
