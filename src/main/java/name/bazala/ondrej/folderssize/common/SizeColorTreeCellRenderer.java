package name.bazala.ondrej.folderssize.common;

import javax.swing.*;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.*;

public class SizeColorTreeCellRenderer extends DefaultTreeCellRenderer {

    private int minForRed;
    private boolean coloring;

    public SizeColorTreeCellRenderer(boolean coloring, JFormattedTextField formattedTextFieldRedText, boolean isRadioMBSelected) {
        this.coloring = coloring;
        minForRed = Integer.parseInt(formattedTextFieldRedText.getText().replaceAll("\\W+", ""));
        if (!isRadioMBSelected) {
            minForRed *= 1024;
        }
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value,
                                                  boolean sel, boolean exp, boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, sel, exp, leaf, row, hasFocus);

        String node = value.toString();
        boolean isMB = node.endsWith("MB");

        this.setClosedIcon(new ImageIcon(getClass().getResource("/icons/folder-closed-icon.png")));
        this.setOpenIcon(new ImageIcon(getClass().getResource("/icons/folder-opened-icon.png")));
        this.setLeafIcon(new ImageIcon(getClass().getResource("/icons/folder-leaf-icon.png")));

        node = node.split("\\s{5}")[1];
        node = node.split(",")[0];
        int num = Integer.parseInt(node);

        setForeground(Color.BLACK);
        setBackgroundSelectionColor(new Color(204, 232, 254));


        if (!coloring) {
            return this;
        }

        if (num >= minForRed) {
            setForeground(Color.RED);
        }

        return this;
    }

    public void setMinForRed(int min) {
        minForRed = min;
    }


    public int getMinForRed() {
        return minForRed;
    }
}
