package name.bazala.ondrej.folderssize.common;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

public class JFileChooserExistingFile extends JFileChooser {

        public JFileChooserExistingFile() {
            FileNameExtensionFilter filterXlsx = new FileNameExtensionFilter("XLSX (Excel) (*.xlsx)", "xlsx");
            FileNameExtensionFilter filterCsv = new FileNameExtensionFilter("CSV (semicolon separated) (*.csv)", "csv");
            this.addChoosableFileFilter(filterXlsx);
            this.addChoosableFileFilter(filterCsv);
            this.setAcceptAllFileFilterUsed(false);
        }

        @Override
        public void approveSelection() {
            setSelectedFile(repairExtension(getSelectedFile()));
            File f = getSelectedFile();
            if(f.exists() && getDialogType() == SAVE_DIALOG){
                int result = JOptionPane.showConfirmDialog(this,"The file exists, overwrite?","Existing file",JOptionPane.YES_NO_CANCEL_OPTION);
                switch(result){
                    case JOptionPane.YES_OPTION:
                        super.approveSelection();
                        return;
                    case JOptionPane.NO_OPTION:
                    case JOptionPane.CLOSED_OPTION:
                        return;
                    case JOptionPane.CANCEL_OPTION:
                        cancelSelection();
                        return;
                }
            }
            super.approveSelection();
        }

        private File repairExtension(File file) {
            FileNameExtensionFilter filter = (FileNameExtensionFilter) this.getFileFilter();
            String correctExtension = '.' + filter.getExtensions()[0];
            String wrongExtension = (correctExtension.equals(".xlsx") ? ".csv" : ".xlsx");
            String fileName = file.toString();

            if (fileName.endsWith(correctExtension)) {
                return new File(fileName);
            }

            if (fileName.endsWith(wrongExtension)) {
                fileName = fileName.replace(wrongExtension, correctExtension);
            } else {
                fileName = fileName.concat(correctExtension);
            }

            return new File(fileName);
        }
}
