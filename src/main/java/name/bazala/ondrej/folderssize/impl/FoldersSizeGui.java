package name.bazala.ondrej.folderssize.impl;


import name.bazala.ondrej.folderssize.common.JFileChooserExistingFile;
import name.bazala.ondrej.folderssize.common.SizeColorTreeCellRenderer;

import javax.swing.*;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class FoldersSizeGui {
    private static final String TREE_THREAD_NAME = "ToTreeThread";
    private static final String FILE_THREAD_NAME = "ToFileThread";

    private JPanel panelMain;
    private JButton buttonShowTree;
    private JTree treeFiles;
    private JComboBox comboBoxRoot;
    private JLabel labelRoot;
    private JLabel labelMinSize;
    private JLabel labelRedSizeType;
    private JRadioButton radioButtonMB;
    private JRadioButton radioButtonGB;
    private JFormattedTextField formattedTextFieldMinSize;
    private JProgressBar progressBarShowTree;
    private JLabel labelShowTree;
    private JButton buttonExportToFile;
    private JProgressBar progressBarExportToFile;
    private JLabel labelExportToFile;
    private JButton buttonShowTreeCancel;
    private JButton buttonExportToFileCancel;
    private JFormattedTextField formattedTextFieldRedText;
    private JPanel panelExportToFile;
    private JPanel panelShowTree;
    private JLabel labelRed;
    private JCheckBox checkBoxUseHighlighting;
    private JPanel panelHighlight;
    private JFileChooser fileChooser;
    private JFrame frame;

    private FoldersSizeService foldersSizeService;
    private NumberFormatter formatter;

    private FoldersSizeGui() {
        foldersSizeService = new FoldersSizeService();
        initialize();
        setListeners();
    }

    private void initialize() {
        UIManager.put("PopupMenu.consumeEventOnClose", false);

        frame = new JFrame("Folders Size");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(panelMain);
        frame.setMinimumSize(new Dimension(400, 300));
        frame.setSize(new Dimension(600, 600));
        frame.setLocationRelativeTo(null);
        frame.setIconImage(new ImageIcon(getClass().getResource("/icons/folder-icon.png")).getImage());
        frame.getRootPane().setDefaultButton(buttonShowTree);
        for (File file : File.listRoots()) {
            comboBoxRoot.addItem(file.getAbsolutePath());
        }
        fileChooser = new JFileChooserExistingFile();
        treeFiles.setCellRenderer(new SizeColorTreeCellRenderer(true, formattedTextFieldRedText, radioButtonMB.isSelected()));
        panelShowTree.setBorder(BorderFactory.createEmptyBorder(0, 0, 46, 0));
        ToolTipManager.sharedInstance().setDismissDelay(Integer.MAX_VALUE);

        formatter = new NumberFormatter();
        formatter.setValueClass(Integer.class);
        formatter.setMinimum(0);
        formatter.setMaximum(Integer.MAX_VALUE);
        formatter.setCommitsOnValidEdit(true);
        DefaultFormatterFactory factory = new DefaultFormatterFactory(formatter);

        formattedTextFieldMinSize.setFormatterFactory(factory);
        formattedTextFieldMinSize.setValue(1);

        String value = formattedTextFieldRedText.getText();
        formattedTextFieldRedText.setFormatterFactory(factory);
        formattedTextFieldRedText.setValue(Integer.parseInt(value));

        ((DefaultTreeModel) treeFiles.getModel()).setRoot(null);
        frame.setVisible(true);
    }

    private void setListeners() {
        buttonShowTree.addActionListener(e -> buttonShowTreeOnAction());
        buttonExportToFile.addActionListener(e -> buttonExportToFileOnAction());
        buttonShowTreeCancel.addActionListener(e -> buttonCancelOnAction(TREE_THREAD_NAME));
        buttonExportToFileCancel.addActionListener(e -> buttonCancelOnAction(FILE_THREAD_NAME));
        treeFiles.addMouseListener(GuiListeners.getTreeOnRightClickPopUpMenuListener(treeFiles));
        treeFiles.addKeyListener(GuiListeners.getTreeOnDeleteKeyListener(treeFiles));
        frame.addMouseListener(GuiListeners.getFrameMouseListener(comboBoxRoot));
        comboBoxRoot.addMouseListener(GuiListeners.getComboBoxMouseListener(comboBoxRoot));
        formattedTextFieldRedText.getDocument().addDocumentListener(GuiListeners.getFormattedTextFieldColorNumListener(
                formattedTextFieldRedText, labelRedSizeType, treeFiles, checkBoxUseHighlighting, radioButtonMB.isSelected()));
        checkBoxUseHighlighting.addActionListener(e -> checkBoxUseHighlightingOnActionListener());
        radioButtonMB.addActionListener(e -> radioButtonMBOnActionListener());
        radioButtonGB.addActionListener(e -> radioButtonGBOnActionListener());
    }

    private void radioButtonMBOnActionListener() {
        changeOfSizeType(true);
    }

    private void radioButtonGBOnActionListener() {
        changeOfSizeType(false);
    }

    private void changeOfSizeType(boolean isMB) {
        labelRedSizeType.setText(isMB ? "MB" : "GB");
        String redValue = formattedTextFieldRedText.getText().replace(" ", "");
        int parsedRedValue = Integer.parseInt(redValue);
        parsedRedValue = isMB ? parsedRedValue * 1000 : parsedRedValue / 1000;
        parsedRedValue = parsedRedValue == 0 ? 1 : parsedRedValue;
        try {
            formattedTextFieldRedText.setText(formatter.valueToString(parsedRedValue));
        } catch (ParseException e) {
            formattedTextFieldRedText.setText("" + parsedRedValue);
        }
    }

    private void checkBoxUseHighlightingOnActionListener() {
        if (!checkBoxUseHighlighting.isSelected()) {
            treeFiles.setCellRenderer(new SizeColorTreeCellRenderer(false, formattedTextFieldRedText, radioButtonMB.isSelected()));
            formattedTextFieldRedText.setEnabled(false);
            labelRed.setEnabled(false);
            labelRedSizeType.setEnabled(false);
        } else {
            treeFiles.setCellRenderer(new SizeColorTreeCellRenderer(true, formattedTextFieldRedText, radioButtonMB.isSelected()));
            formattedTextFieldRedText.setEnabled(true);
            labelRed.setEnabled(true);
            labelRedSizeType.setEnabled(true);
        }
    }

    private void buttonShowTreeOnAction() {
        setMinSize();
        String rootString = (String) comboBoxRoot.getSelectedItem();
        DefaultMutableTreeNode myRoot = new DefaultMutableTreeNode(rootString);
        DefaultTreeModel treeModel = (DefaultTreeModel) treeFiles.getModel();

        frame.getRootPane().setDefaultButton(buttonShowTreeCancel);
        progressBarShowTree.setVisible(true);
        labelShowTree.setVisible(true);
        buttonShowTreeCancel.setVisible(true);
        buttonShowTree.setVisible(false);

        ExecutorService executor = Executors.newCachedThreadPool();
        executor.execute(() -> threadShowTree(myRoot, treeModel));
        executor.shutdown();
    }

    private void threadShowTree(DefaultMutableTreeNode myRoot, DefaultTreeModel treeModel) {
        Thread.currentThread().setName(TREE_THREAD_NAME);
        foldersSizeService.foldersSizeToTree(myRoot, treeModel);
        frame.getRootPane().setDefaultButton(buttonShowTree);
        progressBarShowTree.setVisible(false);
        labelShowTree.setVisible(false);
        buttonShowTreeCancel.setVisible(false);
        buttonShowTree.setVisible(true);
        treeModel.reload();
        if (treeFiles.getModel().getRoot() != null) {
            String labelText = treeFiles.getModel().getRoot().toString().endsWith("MB") ? "MB" : "GB";
            labelRedSizeType.setText(labelText);
            treeFiles.setCellRenderer(new SizeColorTreeCellRenderer(checkBoxUseHighlighting.isSelected(), formattedTextFieldRedText, radioButtonMB.isSelected()));
        }
    }

    private void buttonExportToFileOnAction() {
        String root = (String) comboBoxRoot.getSelectedItem();
        root = root == null ? "hard disk" : root;
        setMinSize();

        double minSize = foldersSizeService.getMinSize() / 1024 / 1024;
        minSize = foldersSizeService.isSizeMB() ? minSize : minSize / 1024;
        String fileMinSize = String.format("%.0f", minSize) + (foldersSizeService.isSizeMB() ? "MB" : "GB");
        String fileName = root.replaceAll("\\W", "");
        fileName = fileName + " - folders size (min " + fileMinSize + ")";
        fileChooser.setSelectedFile(new File(fileName));
        int result = fileChooser.showSaveDialog(panelMain);

        if (result == JFileChooser.CANCEL_OPTION) {
            return;
        }

        File file = fileChooser.getSelectedFile();
        panelShowTree.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
        progressBarExportToFile.setVisible(true);
        labelExportToFile.setVisible(true);
        buttonExportToFileCancel.setVisible(true);
        buttonExportToFile.setVisible(false);
        ExecutorService executor = Executors.newCachedThreadPool();

        String finalRoot = root;
        executor.execute(() -> threadExportToFile(file, finalRoot));
        executor.shutdown();
    }

    private void threadExportToFile(File file, String finalRoot) {
        Thread.currentThread().setName(FILE_THREAD_NAME);
        boolean successful = false;
        try {
            foldersSizeService.foldersSizeToFile(finalRoot, file);
            successful = true;
        } catch (InterruptedException e) {
            if (!file.delete()) {
                JOptionPane.showMessageDialog(null, "Interrupted file was not successfully deleted.", "Error", JOptionPane.WARNING_MESSAGE);
            }
        } catch (IOException e) {
            e.printStackTrace();
            if (!file.delete()) {
                JOptionPane.showMessageDialog(null, "Interrupted file was not successfully deleted.", "Error", JOptionPane.WARNING_MESSAGE);
            }
        } finally {
            panelShowTree.setBorder(BorderFactory.createEmptyBorder(0, 0, 46, 0));
            progressBarExportToFile.setVisible(false);
            labelExportToFile.setVisible(false);
            buttonExportToFileCancel.setVisible(false);
            buttonExportToFile.setVisible(true);
        }
        if (successful) {
            JOptionPane.showMessageDialog(panelMain, "Exporting to file was successful.", "file saved", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private void buttonCancelOnAction(String threadName) {
        Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
        for (Thread thread : threadSet) {
            if (thread.getName().equals(threadName)) {
                thread.interrupt();
            }
        }
    }

    private void setMinSize() {
        String formattedText = formattedTextFieldMinSize.getText();
        double size;
        if (formattedText.isEmpty()) {
            size = 1024 * 1024;
        } else {
            size = (Double.parseDouble(formattedText.replaceAll("\\W+", "")) * 1024) * 1024;
        }

        size = radioButtonGB.isSelected() ? size * 1024 : size;
        foldersSizeService.setMinSize(size);
        foldersSizeService.setSizeMB(radioButtonMB.isSelected());
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Throwable e) {
            e.printStackTrace();
        }

        new FoldersSizeGui();
    }
}
