package name.bazala.ondrej.folderssize.impl;

import name.bazala.ondrej.folderssize.common.SizeColorTreeCellRenderer;
import name.bazala.ondrej.folderssize.filevisitors.DeleteAllFileVisitor;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.Enumeration;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class GuiListeners {
    static MouseListener getTreeOnRightClickPopUpMenuListener(JTree treeFiles) {
        final int[] row = new int[1];
        return new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                row[0] = treeFiles.getRowForLocation(e.getX(), e.getY());
                if (!(SwingUtilities.isRightMouseButton(e) && row[0] != -1)) {
                    return;
                }
                treeFiles.setSelectionRow(row[0]);

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                int newRow = treeFiles.getRowForLocation(e.getX(), e.getY());
                if (!(SwingUtilities.isRightMouseButton(e) && newRow != -1)) {
                    return;
                }
                if (newRow != row[0]) {
                    return;
                }

                treeFiles.setSelectionRow(newRow);
                JPopupMenu menu = new JPopupMenu();
                JMenuItem itemShow = new JMenuItem("show in file explorer", new ImageIcon(getClass().getResource("/icons/search-icon.png")));
                JMenuItem itemDelete = new JMenuItem("delete folder", new ImageIcon(getClass().getResource("/icons/trash-icon.png")));
                menu.add(itemShow);
                menu.add(itemDelete);
                ExecutorService executor = Executors.newCachedThreadPool();
                itemShow.addActionListener(a -> {
                    try {
                        File file = new File(getFolderRealPath(treeFiles));
                        if (!file.exists()) throw new IOException();
                        Desktop.getDesktop().open(file);
                    } catch (IOException ex) {
                        JOptionPane.showMessageDialog(null,"Couldn't open folder.", "Error", JOptionPane.ERROR_MESSAGE);
                        ex.printStackTrace();
                    }});
                itemDelete.addActionListener(a -> executor.execute(() -> folderDelete(treeFiles)));
                menu.show(e.getComponent(), e.getX(), e.getY());
            }
        };
    }

    static DocumentListener getFormattedTextFieldColorNumListener(JFormattedTextField formattedTextFieldRedText,
                                                                  JLabel labelRedSizeType, JTree treeFiles, JCheckBox checkBox, boolean isRadioMBSelected) {
        return new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) { changeRenderer(); }

            @Override
            public void removeUpdate(DocumentEvent e) { changeRenderer(); }

            @Override
            public void changedUpdate(DocumentEvent e) { }

            private void changeRenderer() {
                String redString = formattedTextFieldRedText.getText().replaceAll("\\W+", "");

                if (!checkBox.isSelected() || redString.isEmpty()) {
                    return;
                }

                int redNum = Integer.parseInt(redString);

                if (labelRedSizeType.getText().startsWith("G")) {
                    redNum = redNum * 1024;
                }

                treeFiles.setCellRenderer(new SizeColorTreeCellRenderer(true, formattedTextFieldRedText, isRadioMBSelected));
            }

        };
    }

    static KeyListener getTreeOnDeleteKeyListener(JTree treeFiles) {
        return new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (!treeFiles.isSelectionEmpty() && e.getKeyCode() == KeyEvent.VK_DELETE) {
                    folderDelete(treeFiles);
                }
            }
        };
    }

    static MouseListener getFrameMouseListener(JComboBox comboBoxRoot) {
        return new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                fillComboBox(comboBoxRoot);
            }
        };
    }

    static MouseListener getComboBoxMouseListener(JComboBox comboBoxRoot) {
        return new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                fillComboBox(comboBoxRoot);
            }
        };
    }

    private static void fillComboBox(JComboBox comboBoxRoot) {
        comboBoxRoot.removeAllItems();
        for (File file : File.listRoots()) {
            comboBoxRoot.addItem(file.getAbsolutePath());
        }
    }

    private static void folderDelete(JTree treeFiles) {
        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) treeFiles.getLastSelectedPathComponent();
        String nodeName = selectedNode.toString().split("\\s{5}\\d+,?\\d*\\s[GM]B")[0];
        int decision = JOptionPane.showConfirmDialog(null,"Are you sure you want to delete folder \"" + nodeName + "\"?", "Delete folder", JOptionPane.YES_NO_OPTION);
        if (decision == JOptionPane.YES_OPTION) {
            if (!(deleteNonEmptyFolder(new File(getFolderRealPath(treeFiles))))) {
                JOptionPane.showMessageDialog(null,"Couldn't delete whole folder \"" + nodeName + "\". \nSome files may have been deleted.", "Folder not deleted", JOptionPane.ERROR_MESSAGE);
                return;
            }
            selectedNode.removeFromParent();
            TreePath path = treeFiles.getPathForRow(0);
            Enumeration<TreePath> expandedDescendants = treeFiles.getExpandedDescendants(path);
            ((DefaultTreeModel) treeFiles.getModel()).reload(selectedNode);
            while (expandedDescendants.hasMoreElements()) {
                treeFiles.expandPath(expandedDescendants.nextElement());
            }
        }
    }

    private static boolean deleteNonEmptyFolder(File root) {
        DeleteAllFileVisitor visitor = new DeleteAllFileVisitor();
        try {
            Files.walkFileTree(Paths.get(root.getPath()), visitor);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private static String getFolderRealPath(JTree treeFiles) {
        String path = treeFiles.getSelectionPath().toString();
        String[] folders = path.split("(\\s{5}\\d+,?\\d*\\s[GM]B,?\\s?]?)|\\[");
        StringBuilder sb = new StringBuilder();
        String slash = "";
        for (String folder : folders) {
            sb.append(folder);
            sb.append(slash);
            slash = "\\";
        }
        return sb.toString();
    }
}
