package name.bazala.ondrej.folderssize.impl;

import name.bazala.ondrej.folderssize.filevisitors.ToCsvSizeFileVisitor;
import name.bazala.ondrej.folderssize.filevisitors.ToTreeSizeFileVisitor;
import name.bazala.ondrej.folderssize.filevisitors.ToXlsxSizeFileVisitor;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.io.*;
import java.nio.channels.ClosedByInterruptException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

class FoldersSizeService {
    private double minSize = 0;
    private boolean isSizeMB;

    void foldersSizeToTree(DefaultMutableTreeNode treeNode, DefaultTreeModel treeModel) {
        try {
            ToTreeSizeFileVisitor visitor = new ToTreeSizeFileVisitor(minSize, isSizeMB, treeNode);
            Files.walkFileTree(Paths.get(treeNode.toString()), visitor);
            DefaultMutableTreeNode node = null;
            if (visitor.getRootSize() > minSize) {
                node = visitor.getRoot();
            }
            treeModel.setRoot(node);
        } catch (ClosedByInterruptException e) {

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void foldersSizeToFile(String root, File saveFile) throws InterruptedException, IOException {
        try {
            if (saveFile.toString().endsWith("csv")) {
                csvFile(root, saveFile);
            } else {
                xlsxFile(root, saveFile);
            }
        } catch (ClosedByInterruptException e) {
            throw new InterruptedException();
        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException(e);
        }
    }

    private void csvFile(String root, File saveFile) throws IOException {
        try (OutputStreamWriter writer =
                     new OutputStreamWriter(new FileOutputStream(saveFile), StandardCharsets.UTF_8)) {
            writer.write("folder path;size of folder;unit of size");
            ToCsvSizeFileVisitor visitor = new ToCsvSizeFileVisitor(minSize, isSizeMB, writer);
            Files.walkFileTree(Paths.get(root), visitor);
        }
    }

    private void xlsxFile(String root, File saveFile) throws IOException {
        try (FileOutputStream out = new FileOutputStream(saveFile);
             Workbook workbook = new XSSFWorkbook()) {
            Sheet sheet = workbook.createSheet("Folders Size of " + root.charAt(0));

            ToXlsxSizeFileVisitor visitor = new ToXlsxSizeFileVisitor(minSize, isSizeMB, sheet);
            Files.walkFileTree(Paths.get(root), visitor);

            workbook.write(out);
        }
    }

    void setMinSize(double minSize) {
        this.minSize = minSize;
    }

    void setSizeMB(boolean sizeMB) {
        isSizeMB = sizeMB;
    }

    double getMinSize() {
        return minSize;
    }

    boolean isSizeMB() {
        return isSizeMB;
    }
}
