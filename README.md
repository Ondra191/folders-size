# Folders size

Desktop GUI application for comfortable 
locating of big size folders in computer store space.

- Highlighting of folders with size bigger than specified value

- Ignoring of folders with size smaller than specified value.

- Deleting of folders or opening certain folder in file explorer
  
- Possibility to export folders and their size to CSV or Excel
